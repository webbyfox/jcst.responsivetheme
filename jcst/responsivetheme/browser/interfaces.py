from plone.theme.interfaces import IDefaultPloneLayer
from zope.viewlet.interfaces import IViewletManager

class IThemeSpecific(IDefaultPloneLayer):
    """Marker interface that defines a Zope 3 browser layer.
       If you need to register a viewlet only for the
       "jcst" theme, this interface must be its layer
       (in responsivetheme/viewlets/configure.zcml).
    """
class IJCSTPortalTop(IViewletManager):
    """A viewlet manager that sits at the very top of the rendered page
    """